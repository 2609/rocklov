#language: pt

Funcionalidade: Login
    Sendo um usuário cadastrado
    Quero acessar o sistema da Rocklov
    Para que eu possa anunciar meus equipamentos musicais

    @login
    Cenario: Login do usuário

        Dado que acesso a página principal
        Quando submeto minhas credenciais com "menezes@yahoo.com" e "qaninja"
        Então sou redirecionado para o Dashboard

    Esquema do Cenario: Tentar logar

        Dado que acesso a página principal
        Quando submeto minhas credenciais com "<email_input>" e "<senha_input>"
        Então vejo a mensagem de alerta: "<mensagem_output>"

        Exemplos:
        |email_input      |senha_input|mensagem_output                 |
        |menezes@yahoo.com|abc123     |Usuário e/ou senha inválidos.   |
        |menezes@404.com  |qaninja    |Usuário e/ou senha inválidos.   |
        |marisa&aol.com   |qaninja    |Oops. Informe um email válido!  |
        |                 |qaninja    |Oops. Informe um email válido!  |
        |menezes@yahoo.com|           |Oops. Informe sua senha secreta!|

