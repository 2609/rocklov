#language: pt

Funcionalidade: Cadastro

    Sendo um músico que possui equipamentos musicais
    Quero fazer o meu cadastro no RockLov
    Para que eu possa disponibilizá-los para locação


    @cadastro
    Cenario: Fazer cadastro

        Dado que acesso a página de cadastro
        Quando submeto o seguinte formulário de cadastro:
            | nome           | email            | senha   |
            | Marisa Menezes | marisa@gmail.com | qaninja |
        Então sou redirecionado para o Dashboard

    Esquema do Cenario: Tentativa de cadastro

        Dado que acesso a página de cadastro
        Quando submeto o seguinte formulário de cadastro:
            | nome         | email         | senha         |
            | <nome_input> | <email_input> | <senha_input> |
        Então vejo a mensagem de alerta: "<mensagem_output>"

        Exemplos:
            | nome_input     | email_input      | senha_input | mensagem_output                  |
            |                | marisa@gmail.com | abc123      | Oops. Informe seu nome completo! |
            | Marisa Menezes |                  | scs123      | Oops. Informe um email válido!   |
            | Marisa Menezes | marisa*gmail.com | kjk655      | Oops. Informe um email válido!   |
            | Marisa Menezes | marisa&gmail.com | kjk655      | Oops. Informe um email válido!   |
            | Marisa Menezes | marisa@gmail.com |             | Oops. Informe sua senha secreta! |


